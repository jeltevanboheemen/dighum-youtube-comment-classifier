# dighum/youtube comment classifier

Simpele classifier voor youtube comments.
Verzamelt comments (max 20 per video) voor max 50 videos voor een search query.
Classificeert [positief|negatief].
Getrained op filmreviews, dus geen geweldige score, meer demonstratie van API gebruik.

sentiment_classifier.py om het programma te laten lopen.
